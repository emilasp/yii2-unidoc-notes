<?php
namespace emilasp\notes\models;

use emilasp\users\common\models\User;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "unidoc_tasks_task".
 *
 * @property integer  $id
 * @property integer  $strategy_id
 * @property integer  $project_id
 * @property integer  $parent_id
 * @property string   $name
 * @property string   $description
 * @property string   $result
 * @property string   $checklist
 * @property integer  $progress
 * @property integer  $priority
 * @property integer  $task_type
 * @property integer  $type
 * @property string   $price
 * @property integer  $time
 * @property integer  $status
 * @property string   $started_at
 * @property string   $finished_at
 * @property string   $created_at
 * @property string   $updated_at
 * @property integer  $created_by
 * @property integer  $updated_by
 *
 * @property Strategy $strategy
 * @property Project  $project
 * @property Note     $parent
 * @property Note[]   $tasks
 * @property User     $createdBy
 * @property User     $updatedBy
 */
class Note extends \emilasp\unidoc\models\Task
{
    public static $taskType = Note::TASK_TYPE_NOTE;

    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge([], parent::behaviors());
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['task_type', 'default', 'value' => static::$taskType],
            [['strategy_id', 'name', 'status'], 'required'],
            [
                [
                    'strategy_id',
                    'project_id',
                    'parent_id',
                    'progress',
                    'priority',
                    'type',
                    'status',
                    'created_by',
                    'updated_by'
                ],
                'integer'
            ],
            [['description', 'time', 'result', 'checklist'], 'string'],
            [['price'], 'number'],
            [['started_at', 'finished_at', 'created_at', 'updated_at'], 'safe'],
            [['name'], 'string', 'max' => 255],
            [
                ['project_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => Project::className(),
                'targetAttribute' => ['project_id' => 'id']
            ],
            [
                ['strategy_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => Strategy::className(),
                'targetAttribute' => ['strategy_id' => 'id']
            ],
            [
                ['parent_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => Note::className(),
                'targetAttribute' => ['parent_id' => 'id']
            ],
            [
                ['created_by'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => User::className(),
                'targetAttribute' => ['created_by' => 'id']
            ],
            [
                ['updated_by'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => User::className(),
                'targetAttribute' => ['updated_by' => 'id']
            ],
            ['formTags', 'safe'],
            [['monthDays', 'weekDays', 'customDates',], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return ArrayHelper::merge([], parent::attributeLabels());
    }
}
