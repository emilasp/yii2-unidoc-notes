<?php

namespace emilasp\notes\models;

use emilasp\users\common\models\User;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "unidoc_tasks_strategy".
 *
 * @property integer   $id
 * @property string    $name
 * @property string    $description
 * @property integer   $status
 * @property string    $created_at
 * @property string    $updated_at
 * @property integer   $created_by
 * @property integer   $updated_by
 *
 * @property Project[] $projects
 * @property Note[]    $tasks
 * @property User      $createdBy
 * @property User      $updatedBy
 */
class Strategy extends \emilasp\unidoc\models\Strategy
{
    public static $taskType = Note::TASK_TYPE_NOTE;

    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge([], parent::behaviors());
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'status'], 'required'],
            [['description'], 'string'],
            [['status', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['name'], 'string', 'max' => 255],
            [
                ['created_by'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => User::className(),
                'targetAttribute' => ['created_by' => 'id']
            ],
            [
                ['updated_by'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => User::className(),
                'targetAttribute' => ['updated_by' => 'id']
            ],
            ['formTags', 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return ArrayHelper::merge([], parent::attributeLabels());
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTasks()
    {
        return $this->hasMany(Note::className(), ['strategy_id' => 'id'])->byStatus();
    }
}
