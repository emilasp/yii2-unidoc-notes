<?php

namespace emilasp\notes\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use emilasp\notes\models\Note;

/**
 * NoteSearch represents the model behind the search form of `emilasp\tasks\models\Task`.
 */
class NoteSearch extends Note
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'id',
                    'strategy_id',
                    'project_id',
                    'parent_id',
                    'progress',
                    'priority',
                    'type',
                    'time',
                    'status',
                    'created_by',
                    'updated_by'
                ],
                'integer'
            ],
            [
                [
                    'name',
                    'description',
                    'result',
                    'checklist',
                    'started_at',
                    'finished_at',
                    'created_at',
                    'updated_at'
                ],
                'safe'
            ],
            [['price'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Note::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'task_type'   => static::$taskType,
            'id'          => $this->id,
            'strategy_id' => $this->strategy_id,
            'project_id'  => $this->project_id,
            'parent_id'   => $this->parent_id,
            'progress'    => $this->progress,
            'priority'    => $this->priority,
            'type'        => $this->type,
            'price'       => $this->price,
            'time'        => $this->time,
            'status'      => $this->status,
            'started_at'  => $this->started_at,
            'finished_at' => $this->finished_at,
            'created_at'  => $this->created_at,
            'updated_at'  => $this->updated_at,
            'created_by'  => $this->created_by,
            'updated_by'  => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'result', $this->result])
            ->andFilterWhere(['like', 'checklist', $this->checklist]);

        return $dataProvider;
    }
}
