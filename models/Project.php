<?php

namespace emilasp\notes\models;

use emilasp\users\common\models\User;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "unidoc_tasks_project".
 *
 * @property integer  $id
 * @property integer  $strategy_id
 * @property string   $name
 * @property string   $description
 * @property integer  $progress
 * @property integer  $type
 * @property integer  $status
 * @property string   $created_at
 * @property string   $updated_at
 * @property integer  $created_by
 * @property integer  $updated_by
 *
 * @property Strategy $strategy
 * @property User     $createdBy
 * @property User     $updatedBy
 * @property Note[]   $tasks
 */
class Project extends \emilasp\unidoc\models\Project
{
    public static $taskType = Note::TASK_TYPE_NOTE;

    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge([], parent::behaviors());
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['strategy_id', 'name', 'status'], 'required'],
            [['strategy_id', 'progress', 'type', 'status', 'created_by', 'updated_by'], 'integer'],
            [['description'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['name'], 'string', 'max' => 255],
            [
                ['strategy_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => Strategy::className(),
                'targetAttribute' => ['strategy_id' => 'id']
            ],
            [
                ['created_by'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => User::className(),
                'targetAttribute' => ['created_by' => 'id']
            ],
            [
                ['updated_by'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => User::className(),
                'targetAttribute' => ['updated_by' => 'id']
            ],
            ['formTags', 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return ArrayHelper::merge([], parent::attributeLabels());
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTasks()
    {
        return $this->hasMany(Note::className(), ['project_id' => 'id']);
    }
}
