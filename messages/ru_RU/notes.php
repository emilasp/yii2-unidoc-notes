<?php
return [
    'Notes'                 => 'Заметки',
    'Note'                  => 'Заметка',
    'Create Note'           => 'Создание заметки',
    'Create first strategy' => 'Сначала создайте свою первую стратегию!',
    'Dashboard'             => 'Рабочий стол',
];
