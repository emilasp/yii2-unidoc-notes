<?php
namespace emilasp\notes\widgets\StickyNotesWidget;

use yii\web\AssetBundle;

/**
 * Class MarkdownProcessorAsset
 * @package emilasp\notes\widgets\StickyNotesWidget
 */
class MarkdownProcessorAsset extends AssetBundle
{
    public $sourcePath = '@bower/marked';

    public $jsOptions = ['position' => 1];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\web\JqueryAsset'
    ];

    public $js = [
        'marked.min.js'
    ];
}