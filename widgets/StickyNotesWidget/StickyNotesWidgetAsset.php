<?php
namespace emilasp\notes\widgets\StickyNotesWidget;

use yii\web\AssetBundle;

/**
 * Class StickyNotesWidgetAsset
 * @package emilasp\notes\widgets\StickyNotesWidget
 */
class StickyNotesWidgetAsset extends AssetBundle
{
    public $sourcePath = __DIR__ . '/assets';

    public $js  = ['sticky-notes.js'];
    public $css = ['sticky-notes.css'];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\web\JqueryAsset',
        'yii\jui\JuiAsset',
        'emilasp\notes\widgets\StickyNotesWidget\MarkdownProcessorAsset',
    ];
}
