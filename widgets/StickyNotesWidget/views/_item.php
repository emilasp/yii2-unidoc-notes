<?php
use emilasp\notes\models\Note;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

?>
<div class="note-item <?= $item['data']['class'] ?>" style="<?= $item['style'] ?>"
     data-expand="<?= $item['data']['expand'] ?>" data-expand-height="<?= $item['data']['data-expand-height'] ?>">
    <?= Html::hiddenInput('note-id', $item['id'], ['data-field' => 'id', 'class' => 'note-input-id']) ?>
    <table class="note-item-head">
        <tr>
            <td width="25px">
                <div class="note-item-drag-btn"><i class="fa fa-bars"></i></div>
            </td>
            <td>
                <div class="note-item-title">
                    <?= Html::textInput(
                        'note-title',
                        $item['name'],
                        ['class' => 'note-input', 'data-field' => 'name']
                    ) ?>
                </div>
            </td>
            <td width="60px">
                <div class="note-item-actions">
                    <span class="note-item-actions-delete"><i class="fa fa-trash"></i></span>
                    <span class="note-item-actions-task"><i class="fa fa-id-card"></i></span>
                    <span class="note-item-actions-colapse">
                        <i class="fa <?= $item['data']['expand'] ? 'fa-minus-square' : 'fa-plus-square' ?>"></i>
                    </span>
                </div>
            </td>
        </tr>
    </table>

    <div class="note-collapsed" style="<?= $item['data']['expand'] ? '' : 'display: none;' ?>">
        <div class="note-item-description">
            <?= Html::textarea('note-description', $item['description'], [
                'class'      => 'note-input',
                'data-field' => 'description'
            ]) ?>
        </div>
        <div class="note-item-description-preview">

        </div>
        <div class="note-item-parent">

            <div class="note-item-parent-two">
                <?= Html::dropDownList('note-strategy', $item['strategy_id'], $strategies, [
                    'class'      => 'note-parent-field note-parent-field-strategy noselect',
                    'data-field' => 'strategy_id'
                ]) ?>

                <?= Html::dropDownList(
                    'note-project',
                    $item['project_id'],
                    ArrayHelper::merge(['' => '----'], $projects),
                    [
                        'class'      => 'note-parent-field note-parent-field-project noselect',
                        'data-field' => 'project_id'
                    ]
                ) ?>
            </div>


            <div class="note-item-parent-three">
                <?= Html::dropDownList('note-type', $item['type'], Note::$types, [
                    'class'      => 'note-parent-field note-parent-field-type noselect',
                    'data-field' => 'type'
                ]) ?>

                <?= Html::dropDownList('note-priority', $item['priority'], Note::$priorities, [
                    'class'      => 'note-parent-field note-parent-field-priority noselect',
                    'data-field' => 'priority'
                ]) ?>

                <?= Html::input(
                    'number',
                    'note-price',
                    $item['price'],
                    [
                        'class'      => 'note-parent-field note-parent-field-price note-input noselect',
                        'data-field' => 'price'
                    ]
                ) ?>
            </div>


        </div>
        <div class="ui-resizable-handles"><i class="fa fa-arrows-alt"></i></div>
    </div>
</div>
