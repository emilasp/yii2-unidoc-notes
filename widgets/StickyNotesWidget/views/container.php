<?php
use emilasp\notes\models\Note;

?>

<div class="sticky-notes-container">

    <div class="note-filters">
        <div class="color-dark-red note-filter" data-field="priority" data-value="<?= Note::PRIORITY_HIGHTEST ?>">
            <i class="fa fa-circle"></i>
        </div>
        <div class="color-red note-filter" data-field="priority" data-value="<?= Note::PRIORITY_HIGHT ?>">
            <i class="fa fa-circle"></i>
        </div>
        <div class="color-light-red note-filter" data-field="priority" data-value="<?= Note::PRIORITY_MIDDLE ?>">
            <i class="fa fa-circle"></i>
        </div>
        <div class="color-green note-filter" data-field="priority" data-value="<?= Note::PRIORITY_LOW ?>">
            <i class="fa fa-circle"></i>
        </div>

        <br/>
        <div class="color-dark-blue note-filter" data-field="type" data-value="<?= Note::TYPE_BISHOP ?>">
            <i class="fa fa-rocket"></i>
        </div>
        <div class="color-dark-blue note-filter" data-field="type" data-value="<?= Note::TYPE_FROG ?>">
            <i class="fa fa-motorcycle"></i>
        </div>
        <div class="color-dark-blue note-filter" data-field="type" data-value="<?= Note::TYPE_NO_TYPE ?>">
            <i class="fa fa-wheelchair-alt"></i>
        </div>

        <br/>
        <div class="color-dark-blue note-filter" data-field="price" data-value="">
            <i class="fa fa-usd"></i>
        </div>
    </div>
    <div class="note-items" data-strategy="<?= $strategyId ?>" data-project="<?= $projectId ?>">

        <?php foreach ($items as $index => $item) : ?>
            <?= $this->render('_item', [
                'item'       => $item,
                'strategies' => $strategies,
                'projects'   => $projects,
            ]) ?>
        <?php endforeach; ?>

    </div>
</div>