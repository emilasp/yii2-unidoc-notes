$('body').on('dblclick', '.sticky-notes-container', function (event) {
    var x         = event.pageX - this.offsetLeft;
    var y         = event.pageY - this.offsetTop;
    var container = $('.note-items');
    x             = x - container.offset().left;
    y             = y - container.offset().top;
    createNewItem(x, y);
});


function setEvents(notes) {
    if (typeof notes === 'undefined') {
        notes = $('.note-item');
    }

    notes.on('dblclick', function (event) {
        event.stopPropagation();
    });

    notes.on('click', function (event) {
        var element = $(this);
        $('.note-item').each(function (index, el) {
            var element = $(el);
            if (element.css('z-index') == 1002) {
                element.css('z-index', 1001)
            }
        });
        element.css('z-index', 1002);
    });

    notes.draggable({
        handle: ".note-item-drag-btn",
        //helper: "clone",
        zIndex: 1000,
        opacity: 0.35,
        addClasses: false,
        classes: {"ui-draggable": "note-highlight"},
        containment: ".sticky-notes-container",
        cursor: "crosshair",
        grid: [10, 10],
        stop: function (event, ui) {
            var element = $(this);

            $('.note-item').css('z-index', 100);
            element.css('z-index', 1000);

            saveItem(element);
        },
        start: function (event, ui) {
            var element = $(this);
        }
    });

    notes.each(function () {
        var $this = $(this);
        $this.resizable({
            containment: ".sticky-notes-container",
            stop: function () {
                saveItem($(this));
            },
            minHeight: 80,
            minWidth: 180,
            handles: {'s': $this.find(".ui-resizable-handles")}
        });
    });

    notes.find('input, textarea, select').on('change', function (event) {
        var element       = $(this);
        var noteContainer = element.closest('.note-item');
        var dropNote      = element.data('field') === 'strategy_id';

        if (element.data('field') === 'priority') {
            noteContainer
                .removeClass('note-default')
                .removeClass('note-success')
                .removeClass('note-danger')
                .removeClass('note-warning');

            switch (element.val()) {
                case '1':
                    noteContainer.addClass('note-default');
                    break;
                case '3':
                    noteContainer.addClass('note-warning');
                    break;
                case '4':
                    noteContainer.addClass('note-danger');
                    break;
            }
        }

        saveItem(noteContainer, dropNote);
    });

    notes.on('click', '.note-item-actions-delete', function (event) {
        if (confirm('Удалить заметку?')) {
            var note = $(this).closest('.note-item');

            deleteNote(note);
        }
    });

    notes.on('click', '.note-item-actions-task', function (event) {
        if (confirm('Преобразовать в задачу?')) {
            var id = $(this).closest('.note-item').find('.note-input-id').val();
            window.location.replace("/notes/dashboard/to-task?id=" + id);
        }
    });

    notes.on('click', '.note-item-actions-colapse', function (event) {
        var collapseButton = $(this);

        var icon = collapseButton.find('i.fa');
        var note = collapseButton.closest('.note-item');

        if (note.data('expand') == 1) {
            icon.removeClass('fa-minus-square').addClass('fa-plus-square');
            note.data('expand', 0);
            note.data('expand-height', note.css('height'));
            note.css('height', 'auto');
        } else {
            icon.removeClass('fa-plus-square').addClass('fa-minus-square');
            note.data('expand', 1);
            note.css('height', note.data('expand-height'));
        }
        note.find('.note-collapsed').slideToggle('slow');

        saveItem(note);
    });

    notes.on('click', '.note-item-description-preview', function () {
        var note     = $(this).closest('.note-item');
        var textarea = note.find('.note-item-description');

        $(this).hide();
        textarea.show();
        textarea.find('textarea').focus();
    });

    notes.find('.note-item-description textarea').blur(function () {
        var note = $(this).closest('.note-item');

        mardownProcessor($(this).parent());
    });

}
$(document).keydown(function (event) {
    var element = $(':focus');
    if (element.hasClass('note-input')) {
        if ((event.ctrlKey || event.metaKey) && event.which == 83) {
            element.change();
            event.preventDefault();
            return false;
        }
    }
});


$('body').on("pjax:end", "#notes-pjax", function () {
    setEvents();

    $('.note-item-description').each(function () {
        mardownProcessor($(this));
    });
});

/**
 * Создаём новый item
 */
function createNewItem(x, y) {
    var container = $('.note-items');
    var emptyRow  = container.find('.note-empty-item');
    var newRow    = emptyRow.clone();

    newRow.removeClass('note-empty-item').addClass('note-new-item');
    container.append(newRow);
    setEvents(newRow);

    var strategyId     = newRow.closest('.note-items').data('strategy');
    var projectId      = newRow.closest('.note-items').data('project');
    var strategySelect = newRow.find('.note-parent-field-strategy');
    var projectSelect  = newRow.find('.note-parent-field-project');
    strategySelect.val(strategyId);
    projectSelect.val(projectId);

    newRow.css('left', x);
    newRow.css('top', y);

    saveItem(newRow);
}


function deleteNote(item) {
    var id = item.find('.note-input-id').val();

    $.ajax({
        type: 'POST',
        url: '/notes/dashboard/delete.html',
        dataType: "json",
        data: $.param({"id": id}),
        success: function (msg) {
            if (msg['status'] == '1') {
                item.remove();
                notice(msg['message'], 'green');

                $.pjax.reload({container: "#notes-strategies-pjax", "timeout": 0, push: false});
            } else {
                notice(msg['message'], 'red');
                item.addClass('panel-danger');
            }
        },
        error: function () {
            notice(msg['message'], 'red');
        }
    });
}


/**
 * Сохраняем item на сервере
 * @param item
 * @param drop
 */
function saveItem(item, drop) {
    var element    = $(item);
    var data       = {};
    var strategyId = element.find('.note-parent-field-strategy').val();

    item.find('input, textarea, select').each(function (index, input) {
        var el = $(input);

        data[el.data('field')] = el.val();
    });
    // ------------------- //

    var container                   = element.closest('.note-items');
    data.data                       = {};
    data.data.coordinate            = {};
    data.data.coordinate.top        = element.offset().top - container.offset().top;
    data.data.coordinate.left       = element.offset().left - container.offset().left;
    data.data.coordinate.width      = Number.parseFloat(element.css('width'));
    data.data.coordinate.height     = Number.parseFloat(element.css('height'));
    data.data.coordinate['z-index'] = element.css('z-index');
    data.data.expand                = element.data('expand');


    if (!data.data.expand) {
        data.data.coordinate.height = Number.parseFloat(element.data('expand-height'));
    }

    $.ajax({
        type: 'POST',
        url: '/notes/dashboard/update.html',
        dataType: "json",
        data: $.param(data),
        success: function (msg) {
            if (msg['status'] == '1') {
                if (!data.id) {
                    element.find('input.note-input-id').val(msg.data.id);
                    element.removeClass('note-new-item');
                    notice(msg['message'], 'green');
                    element.addClass('panel-success');

                    $('.btn-strategy').each(function (index) {
                        var but = $(this);
                        var id  = but.data('id');

                        if (id == strategyId || id == '') {
                            var span  = but.find('span');
                            var count = span.text().replace(/\D/g, '');
                            count++;
                            span.html('(' + count + ')');
                        }
                    });
                }
                if (drop) {
                    element.remove();
                }
            } else {
                notice('Error', 'red');
                element.addClass('panel-danger');
            }
        },
        error: function () {
            notice(msg['message'], 'red');
        }
    });
}

setEvents();


function mardownProcessor(description) {
    var textarea = description.find('textarea');
    var note     = textarea.closest('.note-item');
    var preview  = note.find('.note-item-description-preview');

    preview.html(marked(textarea.val()));

    preview.show();
    description.hide();
}


$('.note-item-description').each(function () {
    mardownProcessor($(this));
});


