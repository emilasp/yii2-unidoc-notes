<?php
namespace emilasp\notes\widgets\StickyNotesWidget;

use emilasp\core\components\base\ActiveRecord;
use emilasp\core\components\base\Widget;
use emilasp\notes\models\Strategy;
use emilasp\notes\models\Note;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/**
 * Виджет формирует выбор дней недели, дней месяца и кастомных дат
 *
 * http://tutorialzine.com/2010/01/sticky-notes-ajax-php-jquery/
 *
 * Class StickyNotesWidget
 * @package emilasp\notes\widgets\StickyNotesWidget
 */
class StickyNotesWidget extends Widget
{
    const TYPE_WEEK_DAYS    = 'week-days';
    const TYPE_MONTH_DAYS   = 'month-days';
    const TYPE_CUSTOM_DATES = 'custom-dates';

    public $type = self::TYPE_MONTH_DAYS;
    public $notes = [];

    public $strategyId;
    public $projectId;
    public $strategies;
    public $projects;

    /** @var array Default ITEM */
    private $defaultItem = [
        'id'          => null,
        'name'        => '',
        'description' => '',
        'state'       => 1,
        'strategy_id' => null,
        'project_id'  => null,
        'price'       => 0,
        'type'        => Note::TYPE_NO_TYPE,
        'priority'    => Note::PRIORITY_MIDDLE,

        'data' => [
            'coordinate' => [
                'left'    => '20',
                'top'     => '40',
                'width'   => '300',
                'height'  => '250',
                'z-index' => 10,
            ],
            'expand'     => '1',
            'class'      => 'panel-default',
        ]

    ];

    public function init()
    {
        parent::init();

        $this->registerAssets();
    }

    public function run()
    {
        echo $this->render('container', [
            'strategyId' => $this->strategyId,
            'projectId' => $this->projectId,
            'items'      => $this->getItems(),
            'strategies' => ArrayHelper::map($this->strategies, 'id', 'name'),
            'projects'   => ArrayHelper::map($this->projects, 'id', 'name'),
        ]);
    }

    /**
     * Registers the needed assets
     */
    protected function registerAssets()
    {
        StickyNotesWidgetAsset::register($this->view);
    }

    /**
     * Получаем список заметок для вывода виджетом
     *
     * @return array
     */
    private function getItems()
    {
        $items = [];

        $items[] = $this->getEmptyItem();

        /** @var ActiveRecord $note */
        foreach ($this->notes as $note) {
            $item = $note->getAttributes([
                'id',
                'name',
                'description',
                'strategy_id',
                'project_id',
                'parent_id',
                'type',
                'priority',
                'price',
                'status'
            ]);

            $item['data'] = json_decode($note->data, true);

            $items[] = ArrayHelper::merge($this->defaultItem, $item);
        }

        foreach ($items as $index => $item) {
            $items[$index]          = $this->setItemClass($item);
            $items[$index]          = $this->setItemStylePx($items[$index]);
            $items[$index]['style'] = Html::cssStyleFromArray($items[$index]['data']['coordinate']);
        }

        return $items;
    }

    /**
     * Добавляем единицу измерения к стилям
     *
     * @param array $item
     * @return array mixed
     */
    private function setItemStylePx($item)
    {
        $pixeledCssName = ['width', 'height', 'top', 'left'];
        foreach ($item['data']['coordinate'] as $index => $value) {
            if (in_array($index, $pixeledCssName)) {
                $item['data']['coordinate'][$index] = $value . 'px';

                if ($index === 'height') {
                    if (!$item['data']['expand']) {
                        $item['data']['coordinate'][$index] = 'auto';
                    }
                    $item['data']['data-expand-height'] = $value;
                }
            }
        }
        return $item;
    }

    /**
     * Формируем пустую заметку
     *
     * @return array
     */
    private function getEmptyItem()
    {
        $defaultItem = $this->defaultItem;
        $defaultItem['data']['class'] .= ' note-empty-item';
        $defaultItem['name'] = Yii::t('notes', 'New note');

        return $defaultItem;
    }

    /**
     * Устанавливаем класс
     *
     * @param array $item
     * @return array
     */
    private function setItemClass($item)
    {
        if ($item['priority'] === Note::PRIORITY_HIGHTEST) {
            $item['data']['class'] .= ' note-danger';
        } elseif ($item['priority'] === Note::PRIORITY_HIGHT) {
            $item['data']['class'] .= ' note-warning';
        } elseif ($item['priority'] === Note::PRIORITY_LOW) {
            $item['data']['class'] .= ' note-default';
        }


        return $item;
    }
}
