<?php

use dosamigos\ckeditor\CKEditor;
use dosamigos\selectize\SelectizeTextInput;
use emilasp\media\extensions\FileInputWidget\FileInputWidget;
use kartik\form\ActiveForm;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model emilasp\notes\models\Strategy */
/* @var $form yii\widgets\ActiveForm */

\conquer\codemirror\CodemirrorAsset::register($this);
?>

<div class="strategy-form">

    <?php $form = ActiveForm::begin([
        'id'          => 'learn-form',
        'fieldConfig' => ['autoPlaceholder' => false],
        'formConfig'  => ['deviceSize' => 'sm']
    ]); ?>

    <?= $form->errorSummary($model, ['header' => '']); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->widget(CKEditor::className(), [
        'options' => ['rows' => 3],
        'preset'  => 'standart',
    ]) ?>

    <?= $form->field($model, 'status', [
        'addon' => [
            'groupOptions' => ['class' => 'input-group-sm'],
            'prepend'      => ['content' => '<i class="fa fa-lock"></i>']
        ]
    ])->dropDownList($model::$statuses) ?>

    <?= $form->field($model, 'formTags')->widget(SelectizeTextInput::className(), [
        'loadUrl' => ['/taxonomy/tag/search'],
        'options' => ['class' => 'form-control'],
        'clientOptions' => [
            'plugins' => ['remove_button', 'restore_on_backspace'],
            'valueField' => 'name',
            'labelField' => 'name',
            'searchField' => ['name'],
            'create' => true,
        ],
    ])->hint('Используйте запятые для разделения меток') ?>

    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title"><?= Yii::t('media', 'Files') ?></h3>
        </div>
        <div class="panel-body">
            <?= FileInputWidget::widget([
                'model'         => $model,
                'type'          => FileInputWidget::TYPE_MULTI,
                'title'         => true,
                'description'   => true,
                'showTitle'     => true,
                'previewHeight' => 20,
            ]) ?>
        </div>
    </div>


    <div class="form-group">
        <?= Html::submitButton(
            $model->isNewRecord ? Yii::t('site', 'Create') : Yii::t('tasks', 'Update'),
            ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']
        ) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
