<?php

use emilasp\notes\models\Strategy;
use kartik\grid\DataColumn;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel emilasp\notes\models\search\StrategySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Html::tag('span', '', ['class' => 'fa fa-list text-primary']) . ' ' . Yii::t('tasks', 'Strategies');
$this->params['breadcrumbs'][] = Yii::t('tasks', 'Strategies');
?>
<div class="strategy-index">

    <p><?= Html::a(Yii::t('tasks', 'Create Strategy'), ['create'], ['class' => 'btn btn-success']) ?></p>

    <?php Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'name',
                'format'    => 'raw',
                'value'     => function ($model, $key, $index, $column) {
                    return Html::a($model->name, ['view', 'id' => $model->id],
                        ['class' => '', 'data-pjax' => 0]);
                },
                'class'     => DataColumn::className(),
                'hAlign'    => GridView::ALIGN_LEFT,
                'vAlign'    => GridView::ALIGN_MIDDLE,
                'width'     => '250px',
            ],
            'description:ntext',
            [
                'attribute' => 'status',
                'class'     => DataColumn::className(),
                'value'     => function ($model, $key, $index, $column) {
                    return Strategy::$statuses[$model->status];
                },
                'filter'    => Strategy::$statuses
            ],
            'created_at',
            [
                'attribute'           => 'created_by',
                'value'               => function ($model) {
                    return ArrayHelper::getValue($model->createdBy, 'username', null);
                },
                'class'               => DataColumn::className(),
                'width'               => '150px',
            ],
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>

</div>
