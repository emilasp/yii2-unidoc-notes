<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model emilasp\notes\models\Strategy */

$this->title = Html::tag('span', '', ['class' => 'fa fa-plus text-success']) . ' ' . Yii::t('tasks', 'Create Strategy');
$this->params['breadcrumbs'][] = ['label' => Yii::t('tasks', 'Strategies'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('tasks', 'Create Strategy');
?>
<div class="strategy-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
