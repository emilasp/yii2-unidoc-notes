<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model emilasp\notes\models\Project */

$this->title = Html::tag('span', '', ['class' => 'fa fa-plus text-success']) . ' ' . Yii::t('tasks', 'Create Project');
$this->params['breadcrumbs'][] = ['label' => Yii::t('tasks', 'Projects'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('tasks', 'Create Project');
?>
<div class="project-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
