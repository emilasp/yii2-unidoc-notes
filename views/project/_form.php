<?php

use dosamigos\ckeditor\CKEditor;
use dosamigos\selectize\SelectizeTextInput;
use emilasp\media\extensions\FileInputWidget\FileInputWidget;
use emilasp\notes\models\Strategy;
use kartik\select2\Select2;
use yii\helpers\Html;
use kartik\form\ActiveForm;
use yii\jui\ProgressBar;

/* @var $this yii\web\View */
/* @var $model emilasp\tasks\models\Project */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="project-form">

    <?php $form = ActiveForm::begin([
        'id'          => 'learn-form',
        'fieldConfig' => ['autoPlaceholder' => false],
        'formConfig'  => ['deviceSize' => 'sm']
    ]); ?>

    <?= $form->errorSummary($model, ['header' => '']); ?>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'strategy_id')->widget(Select2::classname(), [
                'language' => 'ru',
                'data' => Strategy::find()->map()->byCreatedBy()->all(),
                'options' => ['placeholder' => 'Выберите  стратегию..'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]) ?>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label class="control-label">Прогресс</label>
                <?= ProgressBar::widget([
                    'clientOptions' => [
                        'value' => $model->progress,
                    ],
                ]) ?>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'status', [
                'addon' => [
                    'groupOptions' => ['class' => 'input-group-sm'],
                    'prepend'      => ['content' => '<i class="fa fa-lock"></i>']
                ]
            ])->dropDownList($model::$statuses) ?>
        </div>
    </div>

    <?= $form->field($model, 'description')->widget(CKEditor::className(), [
        'options' => ['rows' => 2],
        'preset'  => 'standart',
    ]) ?>

    <?= $form->field($model, 'formTags')->widget(SelectizeTextInput::className(), [
        'loadUrl' => ['/taxonomy/tag/search'],
        'options' => ['class' => 'form-control'],
        'clientOptions' => [
            'plugins' => ['remove_button', 'restore_on_backspace'],
            'valueField' => 'name',
            'labelField' => 'name',
            'searchField' => ['name'],
            'create' => true,
        ],
    ])->hint('Используйте запятые для разделения меток') ?>

    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title"><?= Yii::t('media', 'Files') ?></h3>
        </div>
        <div class="panel-body">
            <?= FileInputWidget::widget([
                'model'         => $model,
                'type'          => FileInputWidget::TYPE_MULTI,
                'title'         => true,
                'description'   => true,
                'showTitle'     => true,
                'previewHeight' => 20,
            ]) ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton(
                $model->isNewRecord ? Yii::t('site', 'Create') : Yii::t('site', 'Update'),
            ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']
        ) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
