<?php
/* @var $this yii\web\View */
use emilasp\taxonomy\extensions\CategorySelectTree\CategorySelectTree;
use yii\web\JsExpression;

/* @var $model emilasp\im\common\models\Good */
/* @var $form yii\widgets\ActiveForm */
?>
<div id="checklist" class="tab-pane fade clearfix">

    <h2><?=Yii::t('tasks','Checklist')?></h2>

    <?= $form->field($model, 'checklists')->textInput() ?>

</div>
