<?php
use dosamigos\selectize\SelectizeTextInput;
use emilasp\core\extensions\CodemirrorWidget\CodemirrorWidget;
use emilasp\media\extensions\FileInputWidget\FileInputWidget;
use emilasp\notes\models\Project;
use emilasp\notes\models\Strategy;
use emilasp\notes\models\Note;
use kartik\widgets\DepDrop;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model emilasp\notes\models\Task */
/* @var $form yii\widgets\ActiveForm */
?>
<div id="base" class="tab-pane fade in active clearfix">

    <div class="row">
        <div class="col-md-6">
            <div class="row">
                <div class="col-md-4">
                    <?= $form->field($model, 'strategy_id')->dropDownList(
                        Strategy::find()->map()->byCreatedBy()->all(),
                        ['id' => 'tasks-strategy_id']
                    ) ?>
                </div>
                <div class="col-md-4">
                    <?= $form->field($model, 'project_id')->widget(DepDrop::classname(), [
                        'options'       => ['id' => 'tasks-project_id'],
                        'data'          => Project::find()
                            ->filterWhere(['strategy_id' => $model->strategy_id])
                            ->map()
                            ->byCreatedBy()
                            ->all(),
                        'pluginOptions' => [
                            'depends'     => ['tasks-strategy_id'],
                            'placeholder' => Yii::t('tasks', 'Select project'),
                            'url'         => Url::to(['/tasks/project/strategy-depend'])
                        ]
                    ]) ?>
                </div>
                <div class="col-md-4">
                    <?= $form->field($model, 'parent_id')->widget(DepDrop::classname(), [
                        'data'           => ArrayHelper::merge(['' => Yii::t('site', 'select')],
                            Note::find()
                                ->filterWhere(['project_id' => $model->project_id])
                                ->map()
                                ->byCreatedBy()
                                ->all()),
                        'type'           => DepDrop::TYPE_SELECT2,
                        'select2Options' => ['pluginOptions' => ['allowClear' => true]],
                        'pluginOptions'  => [
                            'depends'     => ['tasks-strategy_id', 'tasks-project_id'],
                            'placeholder' => Yii::t('tasks', 'Select project'),
                            'url'         => Url::to(['/tasks/task/depend'])
                        ]
                    ]) ?>
                </div>
            </div>


            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'description')->widget(CodemirrorWidget::className(), [
                'type'     => CodemirrorWidget::TYPE_CODE_MARKDOWN,
                'options'  => ['rows' => 20],
                'settings' => ['lineWrapping' => true],
            ]) ?>




            <?= $form->field($model, 'result')->textarea(['rows' => 6]) ?>

        </div>
        <div class="col-md-6">
            <div class="row">
                <div class="col-md-6">
                    <?= $form->field($model, 'type', [
                        'addon' => [
                            'groupOptions' => ['class' => 'input-group-sm'],
                            'prepend'      => ['content' => '<i class="fa fa-lock"></i>']
                        ]
                    ])->dropDownList($model::$types) ?>
                </div>
                <div class="col-md-6">
                    <?= $form->field($model, 'priority', [
                        'addon' => [
                            'groupOptions' => ['class' => 'input-group-sm'],
                            'prepend'      => ['content' => '<i class="fa fa-lock"></i>']
                        ]
                    ])->dropDownList($model::$priorities) ?>
                </div>
            </div>

            <?= $form->field($model, 'status', [
                'addon' => [
                    'groupOptions' => ['class' => 'input-group-sm'],
                    'prepend'      => ['content' => '<i class="fa fa-lock"></i>']
                ]
            ])->dropDownList($model::$statuses) ?>

            <?= $form->field($model, 'price')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'progress')->textInput() ?>

            <?= $form->field($model, 'formTags')->widget(SelectizeTextInput::className(), [
                'loadUrl'       => ['/taxonomy/tag/search'],
                'options'       => ['class' => 'form-control'],
                'clientOptions' => [
                    'plugins'     => ['remove_button', 'restore_on_backspace'],
                    'valueField'  => 'name',
                    'labelField'  => 'name',
                    'searchField' => ['name'],
                    'create'      => true,
                ],
            ])->hint('Используйте запятые для разделения меток') ?>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><?= Yii::t('media', 'Files') ?></h3>
                </div>
                <div class="panel-body">
                    <?= FileInputWidget::widget([
                        'model'         => $model,
                        'type'          => FileInputWidget::TYPE_MULTI,
                        'title'         => true,
                        'description'   => true,
                        'showTitle'     => true,
                        'previewHeight' => 20,
                    ]) ?>
                </div>
            </div>
        </div>
    </div>


</div>