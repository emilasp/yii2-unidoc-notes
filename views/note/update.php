<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model emilasp\notes\models\Task */

$this->title = Html::tag('span', '', ['class' => 'fa fa-pencil text-primary']) . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('notes', 'Notes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('site', 'Update');
?>
<div class="task-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
