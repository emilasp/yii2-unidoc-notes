<?php

use emilasp\notes\models\Project;
use emilasp\notes\models\Strategy;
use emilasp\notes\models\Note;
use kartik\grid\DataColumn;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel emilasp\notes\models\search\TaskSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title                   = Html::tag('span', '', ['class' => 'fa fa-list text-primary']) . ' '
    . Yii::t('notes', 'Notes');
$this->params['breadcrumbs'][] = Yii::t('notes', 'Notes');
?>
<div class="task-index">

    <p><?= Html::a(Yii::t('notes', 'Create Note'), ['create'], ['class' => 'btn btn-success']) ?></p>

    <?php Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [
            ['class' => 'yii\grid\SerialColumn'],

            'name',

            [
                'attribute' => 'strategy_id',
                'class'     => DataColumn::className(),
                'value'     => function ($model, $key, $index, $column) {
                    return $model->strategy->name;
                },
                'filter'    => Strategy::find()->byCreatedBy()->map()->cache()->orderBy('name')->all(),
            ],

            [
                'attribute' => 'project_id',
                'class'     => DataColumn::className(),
                'value'     => function ($model, $key, $index, $column) {
                    return ArrayHelper::getValue($model->project, 'name', null);
                },
                'filter'    => Project::find()->byCreatedBy()->map()->cache()->orderBy('name')->all(),
            ],
            [
                'attribute' => 'parent_id',
                'class'     => DataColumn::className(),
                'value'     => function ($model, $key, $index, $column) {
                    return ArrayHelper::getValue($model->parent, 'name', null);
                },
                'filter'    => Note::find()->byCreatedBy()->map()->cache()->orderBy('name')->all(),
            ],
            [
                'attribute' => 'progress',
                'class'     => DataColumn::className(),
                'value'     => function ($model, $key, $index, $column) {
                    return \yii\jui\ProgressBar::widget(['clientOptions' => ['value' => $model->progress]]);
                },
                'format'    => 'raw',
            ],


            // 'description:ntext',
            // 'result:ntext',
            // 'checklists',

             'price',
             'time:datetime',
             'started_at',
             'finished_at',
            [
                'attribute' => 'type',
                'class'     => DataColumn::className(),
                'value'     => function ($model, $key, $index, $column) {
                    return Note::$types[$model->type];
                },
                'filter'    => Note::$types
            ],
            [
                'attribute' => 'priority',
                'class'     => DataColumn::className(),
                'value'     => function ($model, $key, $index, $column) {
                    return Note::$priorities[$model->priority];
                },
                'filter'    => Note::$priorities
            ],
            [
                'attribute' => 'status',
                'class'     => DataColumn::className(),
                'value'     => function ($model, $key, $index, $column) {
                    return Project::$statuses[$model->status];
                },
                'filter'    => Project::$statuses
            ],
            [
                'attribute' => 'created_by',
                'value'     => function ($model) {
                    return ArrayHelper::getValue($model->createdBy, 'username', null);
                },
                'class'     => DataColumn::className(),
                'width'     => '150px',
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
