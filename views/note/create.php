<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model emilasp\notes\models\Task */

$this->title = Html::tag('span', '', ['class' => 'fa fa-plus text-success']) . ' ' . Yii::t('notes', 'Create Note');
$this->params['breadcrumbs'][] = ['label' => Yii::t('notes', 'Note'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('notes', 'Create Note');
?>
<div class="task-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
