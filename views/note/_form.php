<?php
use kartik\form\ActiveForm;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model emilasp\notes\models\Task */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="task-form">

    <?php $form = ActiveForm::begin([
        'id'          => 'learn-form',
        'fieldConfig' => ['autoPlaceholder' => false],
        'formConfig'  => ['deviceSize' => 'sm']
    ]); ?>

    <?= $form->errorSummary($model, ['header' => '']); ?>

    <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#base"><?= Yii::t('site', 'Tab base') ?></a></li>
        <li><a data-toggle="tab" href="#checklist"><?= Yii::t('tasks', 'Checklist') ?></a></li>
    </ul>

    <div class="tab-content">
        <?= $this->render('tabs/_base', ['form' => $form, 'model' => $model]) ?>
        <?= $this->render('tabs/_checklist', ['form' => $form, 'model' => $model]) ?>
    </div>


    <div class="form-group">
        <?= Html::submitButton(
            $model->isNewRecord ? Yii::t('site', 'Create') : Yii::t('site', 'Update'),
            ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']
        ) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
