<?php

use emilasp\core\assets\OwlCaruselAsset;
use emilasp\notes\widgets\StickyNotesWidget\StickyNotesWidget;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel emilasp\notes\models\search\ProjectSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

OwlCaruselAsset::register($this);

$this->title                   = Html::tag('span', '', ['class' => 'fa fa-list text-primary']) . ' '
    . Yii::t('notes', 'Dashboard');
$this->params['breadcrumbs'][] = Yii::t('notes', 'Dashboard');
?>
<div class="project-index">




    <div class="row">
        <div class="col-md-2">
            <?php Pjax::begin(['id' => 'notes-strategies-pjax', 'options' => ['class' => 'no-loading']]); ?>

            <?php
            $html       = '';
            $totalCount = 0;
            foreach ($strategies as $strategy) {
                $tasksCount = $strategy->getTasksCount();
                $totalCount += $tasksCount;
                $html .= Html::button(
                    $strategy->name . Html::tag('span', '(' . $tasksCount . ')', [
                        'class' => 'float-right'
                    ]),
                    [
                        'class'   => 'btn btn-strategy ' . ($strategy->id == $strategyId ? 'btn-primary' : ''),
                        'data-id' => $strategy->id
                    ]
                );
            }

            $html .= Html::button(
                    Yii::t('tasks', 'All strategies') . Html::tag('span', '(' . $totalCount . ')', [
                        'class' => 'float-right'
                    ]),
                    [
                        'class'   => 'btn btn-strategy ' . (!$strategyId ? 'btn-primary' : ''),
                        'data-id' => ''
                    ]
                );
            echo $html;
            ?>

            <?php Pjax::end(); ?>
        </div>
        <div class="col-md-10">

            <?php Pjax::begin(['id' => 'notes-pjax', 'options' => ['class' => 'no-loading']]); ?>

            <div class="row">
                <div class="col-md-1 btn-owl-prev">
                    <button type="button" class="btn btn-info"><i class="fa fa-arrow-left" aria-hidden="true"></i></button>
                </div>
                <div class="col-md-10">

                    <div class="owl-carousel owl-theme note-filters">

                        <?php foreach ($projects as $index => $project) : ?>

                            <div class="item project-item note-filter" data-hash="project<?= $project->id ?>"
                            data-field="project" data-value="<?= $project->id ?>" data-enabled="0">
                                <?= Html::a($project->name, '#project' . $project->id, [
                                    'class'   => 'btn btn-project btn-default '
                                        . ($project->id == $projectId ? 'btn-info' : ''),
                                    'data-id' => $project->id,
                                ]) ?>
                            </div>

                        <?php endforeach; ?>

                    </div>

                </div>
                <div class="col-md-1 btn-owl-next">
                    <button type="button" class="btn btn-info"><i class="fa fa-arrow-right" aria-hidden="true"></i></button>
                </div>
            </div>

            <?= StickyNotesWidget::widget([
                'strategyId' => $strategyId,
                'projectId'  => $projectId,
                'strategies' => $strategies,
                'projects'   => $projects,
                'notes'      => $notes,
            ]) ?>

            <?php Pjax::end(); ?>
        </div>
    </div>

</div>

<?php
$js = <<<JS
    $('body').on('click', '.btn-strategy', function() {
      var button = $(this);
      var strategy_id = button.data('id');

      $('.note-items').data('strategy', strategy_id);
      
      $('.btn-strategy').removeClass('btn-primary');
      button.addClass('btn-primary');
      window.location.hash = '';
      
      $.pjax({
          container:"#notes-pjax",
          "timeout" : 0,
          push:false,
          "data":{"strategy_id": strategy_id}
      });
    });

    $('body').on('dblclick', '.sticky-notes-container .note-filters > div', function(event) {
      event.stopPropagation();
    });

    $('body').on('click', '.note-filters .note-filter', function(event) {
      var element = $(this);
      var field = element.data('field');
      var value = element.data('value');
      var enabled = element.data('enabled');
      var notes = $('.note-item');
      
      console.log(field);
      console.log(value);
      console.log(enabled);
      
      notes.removeClass('hidden');    
      
      if (enabled == 1) {
          element.data('enabled', '0');
          element.removeClass('note-filter-active');
      } else {
          $('.note-filters .note-filter').data('enabled', '0')
          .removeClass('note-filter-active');
          element.data('enabled', '1');
          element.addClass('note-filter-active');
    
          if (field !== 'price') {
              notes
                .find('.note-parent-field-' + field + ' option[value!="' + value + '"]:selected')
                .closest('.note-item')
                .addClass('hidden');
          } else {
              notes
                .find('.note-parent-field-' + field)
                .filter(function() { return this.value <= 0; })
                .closest('.note-item')
                .addClass('hidden');
          }
      }
    });
    
    $('body').on("pjax:end", "#notes-pjax", function () {
        setOwlCaruselProjects();
    });
    
    function setOwlCaruselProjects() {
         var owl = $('.owl-carousel').owlCarousel({
            loop:false,
            nav:false,
            navigation : false,
            dots:false,
            center:true,
            navText: ['', ''],
            margin:1,
            URLhashListener:true,
            autoplayHoverPause:true,
            startPosition: 'URLHash',
            responsiveClass:true,
            responsive:{
                0:{
                    items:1,
                    nav:true
                },
                600:{
                    items:3,
                    nav:false
                },
                1000:{
                    items:5,
                    nav:true,
                    loop:false
                }
            }
        });
        
        $(".btn-owl-prev").click(function () {
            owl.trigger('prev.owl.carousel');
        });
        
        $(".btn-owl-next").click(function () {
            owl.trigger('next.owl.carousel');
        });
        
        $('.owl-carousel').on('mousewheel', '.owl-stage', function (e) {
            if (e.originalEvent.wheelDelta < 0) {
                $(this).trigger('next.owl');
            } else {
                $(this).trigger('prev.owl');
            }
            e.preventDefault();
        });
    }
    
    setOwlCaruselProjects();
JS;

$this->registerJs($js);
?>
