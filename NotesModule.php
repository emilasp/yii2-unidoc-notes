<?php
namespace emilasp\notes;

use emilasp\unidoc\UnidocModule;

/**
 * Class NoteModule
 * @package emilasp\notes
 */
class NotesModule extends UnidocModule
{
    public $defaultRoute = 'notes';
    public $controllerNamespace = 'emilasp\notes\controllers';


    public function init()
    {
        parent::init();
    }
}
