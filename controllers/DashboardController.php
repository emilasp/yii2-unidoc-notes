<?php

namespace emilasp\notes\controllers;

use emilasp\notes\models\Note;
use emilasp\core\components\base\Controller;
use emilasp\notes\models\Project;
use emilasp\notes\models\Strategy;
use emilasp\tasks\models\Task;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

/**
 * DashBoardController
 */
class DashboardController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only'  => [
                    'index',
                ],
                'rules' => [
                    [
                        'actions' => [
                            'index',
                            'delete',
                            'update',
                            'to-task',
                        ],
                        'allow'   => true,
                        'roles'   => ['@'],
                    ],
                ],
            ],
            'verbs'  => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                    'update' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Task models.
     * @return string
     */
    public function actionIndex()
    {
        $strategyId = Yii::$app->request->get('strategy_id', null);
        $projectId  = Yii::$app->request->get('project_id');

        if (!$strategies = Strategy::find()->byCreatedBy()->byStatus()->all()) {
            Yii::$app->session->setFlash('danger', Yii::t('notes', 'Create first strategy'));
            return $this->redirect('/notes/strategy/create');
        }

        if ($projectId && $project = Project::find()->byCreatedBy()->where(['id' => $projectId])->one()) {
            $strategy   = $project->strategy;
            $strategyId = $strategy->id;
            $projects   = $strategy->projects;
            $items      = $project->tasks;
        } elseif ($strategyId && $strategy = Strategy::find()->byCreatedBy()->where(['id' => $strategyId])->one()) {
            $projects = $strategy->projects;
            $items    = $strategy->tasks;
        } elseif ($strategyId === null) {
            $strategy   = ArrayHelper::getValue($strategies, 0);
            $projects   = $strategy->projects;
            $items      = $strategy->tasks;
            $strategyId = $strategy->id;
        } else {
            $projects = Project::find()->byCreatedBy()->byStatus()->all();
            $items    = Note::find()->byCreatedBy()->byStatus()->all();
        }

        return $this->render('index', [
            'strategyId' => $strategyId,
            'strategies' => $strategies,
            'projectId'  => $projectId,
            'projects'   => $projects,
            'notes'      => $items
        ]);
    }


    /**
     * Lists all Task models.
     * @return mixed
     */
    public function actionDelete()
    {
        $model = null;
        if ($id = Yii::$app->request->post('id')) {
            $model = Note::find()->byCreatedBy()->where(['id' => $id])->one();
        }

        if ($model && $model->deleteSafe()) {
            return $this->setAjaxResponse(self::AJAX_STATUS_SUCCESS, Yii::t('notes', 'Note has been deleted'));
        } else {
            return $this->setAjaxResponse(self::AJAX_STATUS_ERROR, Yii::t('notes', 'Note deleted error'));
        }
    }


    /**
     * Lists all Task models.
     * @return mixed
     */
    public function actionUpdate()
    {
        $id = Yii::$app->request->post('id');

        if ($id) {
            $model = Note::find()->byCreatedBy()->where(['id' => $id])->one();
        } else {
            $model         = new Note();
            $model->status = Note::STATUS_ENABLED;
        }

        $model->setAttributes(Yii::$app->request->post());

        $model->data = json_encode(Yii::$app->request->post('data'));


        if ($model->save()) {
            return $this->setAjaxResponse(
                self::AJAX_STATUS_SUCCESS,
                Yii::t('notes', 'Note has been update'),
                ['id' => $model->id]
            );
        } else {
            return $this->setAjaxResponse(self::AJAX_STATUS_ERROR, Yii::t('notes', 'Note update error'));
        }
    }

    /**
     * Преобразуем заметку в задачу
     *
     * @param int $id
     * @return \yii\web\Response
     */
    public function actionToTask(int $id)
    {
        $task = Task::find()->where(['id' => $id])->one();

        $task->markAttributeDirty('updated_at');
        $task->save();

        return $this->redirect(['/tasks/task/update', 'id' => $id]);
    }
}
